function [out] = error_func(s,i,s2,i2)
    out=0;
    for j=(1:size(s,2))
        out=out+(s(j)-s2(j))^2+(i(j)-i2(j))^2;
    end
    out=sqrt(out);
end

