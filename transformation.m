function [functions] = transformation(functions,scores,S,I,step_size,dim)
    feat = size(functions,2);
    num_func = size(functions,1);
    centroid=zeros([1,feat]);
    alpha=1; gamma=2; rho=.5; 
    for i=(1:num_func-1)
        centroid=centroid+functions(i,:);
    end
    centroid=centroid/(num_func-1);
    %reflection
    xr=centroid+alpha*(centroid-functions(end,:));
    score=compute_func(xr,S,I, step_size,dim);
    if score<scores(end-1) && score>=scores(1)
        functions(end,:)=xr;return

    %expansion
    elseif score<scores(1)
        xe=centroid+gamma*(xr-centroid);
        score2=compute_func(xe,S,I,step_size,dim);
        if score2<score
            functions(end,:)=xe;
        else
            functions(end,:)=xr;
        end
        return
    else
        %contraction
        if score<scores(end)
            xc=centroid+rho*(xr-centroid);
            score_contraction=compute_func(xc,S,I,step_size,dim);
            if score_contraction<score
                functions(end,:)=xc;return;
            else
                functions=shrink(functions); return;
            end
        else
            xc=centroid+rho*(functions(end,:)-centroid);
            score_contraction=compute_func(xc,S,I,step_size,dim);
            if score_contraction<scores(end)
                functions(end,:)=xc; return;
            else 
                functions=shrink(functions); return
            end
        end
    end
end

function [functions] = shrink(functions)
    sigma=.5;
    for i=(2:size(functions,1))
        functions(i,:)=functions(1,:)+sigma*(functions(i,:)-functions(1,:));
    end
end
