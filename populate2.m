function [out] = populate2(terms)
    nt = size(terms,1);
    feats=nt^2;
    out=zeros([feats+1,feats+nt]);
    %let x1 be [1,0,0,0]
    out(1,:)=[0,.1,9,-.1,.1,.1];
    for i=(2:feats+1)
        v=zeros([1,feats]);
        v(i-1)=.05;
        v=reshape(v,[nt,nt]);
        v(:,end+1)=terms;
        out(i,:)=reshape(v',[1,feats+nt]);
    end
end

