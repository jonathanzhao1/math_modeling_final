s0=.9;
i0=.1;
c1=2;
c2=.2;
s=[s0];
i=[i0];
step_size=.1;
while i(end)>0.01
    prev_s=s(end);
    prev_i=i(end);
    s(end+1)=prev_s-(c1*prev_s*prev_i*step_size);
    i(end + 1)=prev_i+((c1*prev_s*prev_i  - c2*prev_i)*step_size);
end
t=(0:size(s,2)-1)*step_size;
[f, s1, i2]=nelder_mead(s,i,step_size);
[f2,s2,i3]=nelder_mead_2(s,i,step_size);

l1=plot(t,i); label1="original I";
hold on
l2=plot(t,i2);label2="nelder mead with 2 features";
hold on
l3=plot(t,i3);label3="nelder mead with 3 features";
legend([l1,l2,l3],[label1,label2,label3])
hold off