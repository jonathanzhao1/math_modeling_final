function [out,s,i] = compute_func(function_to_apply,S,I,step_size,dim)
    func = reshape(function_to_apply,dim)';
    results = zeros([dim(2),size(S,2)]);
    results(:,1)=func(:,end);
    func=func(:,1:end-1);
    for j=(2:(size(S,2)))
        prev=results(:,j-1);
        results(:,j)=prev-func*prev*step_size;
    end
    s=results(1,:);i=results(2,:);
    out=error_func(S,I,s,i);
end

