function [out, new_s,new_i] = nelder_mead(S,I,step_size)
    terms=[.9;.1;0];
    feats = size(terms,1)^2;
    dim = [size(terms,1)+1,size(terms,1)];
    functions=populate(terms);
    for iterator=(1:50)
        scores=zeros([1,feats+1]);
        for i=(1:feats+1)
            scores(i)=compute_func(functions(i,:),S,I,step_size,dim);
        end
        [scores, sortIdx]=sort(scores, 'ascend');
        disp(scores);
        functions=functions(sortIdx,:);
        functions=transformation(functions,scores,S,I,step_size,dim);
    end
    out=functions(1,:);
    [score, new_s,new_i]=compute_func(out,S,I,step_size,dim);
end

